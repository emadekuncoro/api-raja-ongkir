<?php 

require_once 'api.php';

$province = json_decode(get_RajaOngkir("http://api.rajaongkir.com/starter/province"));
if(!empty($province))
{
	$province = $province->rajaongkir->results;
}
else
{
	$province = array();
}

 ?>

 <!DOCTYPE html>
 <html>
 <head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<title>API Raja Ongkir</title>
 	<link rel="stylesheet" href="">
 </head>
 <body>
 		
 	<form id="form" action="">
 	<h3>Origin</h3>
 		<select required name="province" id="">
 			<option value="">Select Province</option>
 			<?php foreach ($province as $value) 
 			{
 				echo '<option value="'.$value->province_id.'">'.$value->province.'</option>';
 			} 

 			?>
 		</select>
 		<select required name="city" id="">
 			<option value="">Select City</option>
 		</select>
 	<h3>Destination</h3>
 		<select required name="province2" id="">
 			<option value="">Select Province</option>
 			<?php foreach ($province as $value) 
 			{
 				echo '<option value="'.$value->province_id.'">'.$value->province.'</option>';
 			} 

 			?>
 		</select>
 		<select required name="city2" id="">
 			<option value="">Select City</option>
 		</select>
 		<br><br>
 		<input required type="text" name="weight" placeholder="gram"> 
 		<button type="submit">Request</button>
 	</form>
 	<br><br>
	<div id="respon"></div>
 </body>
 </html>

<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script>
$(function() {
	
	$('#form').submit(function(event) {
		$('#respon').html('');
		var data = $(this).serialize();
		$.post('api.php?action=getCost', data , function(resp, textStatus, xhr) {
		var obj = JSON.parse(resp);
		$.each(obj, function(index, val) {
			 $('#respon').append(val);
		});


		});
		return false;
	});

});
	$("select[name='province']").change(function(event) {
		var id = $(this).val();
		$("select[name='city']").html('');
		$.get('api.php?action=getCity&province='+id, function(data) {
			$("select[name='city']").html(data);
		});
	});

	$("select[name='province2']").change(function(event) {
		var id = $(this).val();
		$("select[name='city2']").html('');
		$.get('api.php?action=getCity&province='+id, function(data) {
			$("select[name='city2']").html(data);
		});
	});


</script>