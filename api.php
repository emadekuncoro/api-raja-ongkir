<?php 

function get_RajaOngkir($url)
{
	$curl = curl_init();
	$header = array("key: 7af566b8d3dfb2b34d3b634e612021a2");
	
	curl_setopt_array($curl, array(
	  CURLOPT_URL => $url,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => $header,
	));

	return $response = curl_exec($curl);
}


function get_Ongkir($url, $post=null)
{
	$curl = curl_init();
	$header = array("key: 7af566b8d3dfb2b34d3b634e612021a2");
	
	curl_setopt_array($curl, array(
	  CURLOPT_URL => $url,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => $post,
	  CURLOPT_HTTPHEADER => $header,
	));

	return $response = curl_exec($curl);
}


if(isset($_GET['action']) && $_GET['action'] == 'getCity')
{
	$id   = $_GET['province'];
	$city = json_decode(get_RajaOngkir("http://api.rajaongkir.com/starter/city?province=".$id));
	$city = $city->rajaongkir->results;
	$res  = '';
	foreach ($city as $value) 
	{
		$res .= '<option value="'.$value->city_id.'">('. $value->type.') '. $value->city_name.'</option>';
	}

	echo $res;
}

if(isset($_GET['action']) && $_GET['action'] == 'getCost')
{
	$origin = $_POST['city'];
	$dest = $_POST['city2'];
	$weight = $_POST['weight'];
	$post = "origin={$origin}&destination={$dest}&weight={$weight}&courier=jne";
	$cost = json_decode(get_Ongkir("http://api.rajaongkir.com/starter/cost", $post));
	$cost = $cost->rajaongkir->results[0]->costs;
	$res = array();
	foreach ($cost as $key => $value) 
	{
		$res[] = ' - service : '.$value->service.' <br >cost : Rp '.number_format($value->cost[0]->value, 2).' <br> estimation : '.$value->cost[0]->etd.' day(s) <hr>';
	}

	echo json_encode($res);
}

 ?>